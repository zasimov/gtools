/*
 * gdump dumps GPU memory to the file.
 */

#include <stdio.h>
#include <unistd.h>


const size_t KB = 1024;
const size_t MB = 1024 * KB;


#define cudaFatal(expr) { __cudaFatal((expr), __FILE__, __LINE__); }
inline void __cudaFatal(cudaError_t err, const char *file, int lineno) {
  if (err != cudaSuccess) {
    fprintf(stderr, "CUDA: %s %s %d\n", cudaGetErrorString(err), file, lineno);
    exit(err);
  }
}


int main(int argc, char **argv) {
  int block_size = 4 * MB;
  size_t gpu_free = 0;
  size_t gpu_total = 0;
  size_t blocks = 0;
  void **allocated_blocks;
  FILE *out = NULL;
  int exit_code = 0;
  void *host_buf = NULL;
  cudaError_t err;
  char *out_file_name = NULL;
  char c;

  while ((c = getopt(argc, argv, "b:o:")) != -1) {
    switch (c) {
    case 'o':
      out_file_name = optarg;
      break;
    case 'b':
      block_size = atoi(optarg);
      break;
    default:
      abort();
    }
  }

  if (!out_file_name) {
    fprintf(stderr, "ERROR: output file (-o) is required.\n");
    return 1;
  }

  if (block_size <= 0) {
    fprintf(stderr, "ERROR: block size must be a positive number.\n");
    return 1;
  }

  host_buf = (void *)malloc(block_size);
  if (!host_buf) {
    perror("malloc");
    return 1;
  }

  err = cudaMemGetInfo(&gpu_free, &gpu_total);
  cudaFatal(err);

  blocks = gpu_free / block_size;

  printf("cudaMemGetInfo: free %d, total %d, blocks %d.\n", gpu_free, gpu_total, blocks);

  allocated_blocks = (void **)calloc(blocks, sizeof(void *));
  if (!allocated_blocks) {
    perror("calloc");
    return 1;
  }

  for (int i = 0; i < blocks; i++) {
    err = cudaMalloc(&allocated_blocks[i], block_size);
    cudaFatal(err);
  }

  out = fopen(out_file_name, "wb+");
  if (!out) {
    perror("fopen");
    exit_code = 1;
    goto free;
  }

  for (int i = 0; i < blocks; i++) {
    err = cudaMemcpy(host_buf, allocated_blocks[i], block_size, cudaMemcpyDeviceToHost);
    cudaFatal(err);
    size_t written = fwrite(host_buf, block_size, 1, out);
    if (written != 1) {
      perror("fwrite");
      exit_code = 1;
      goto close_and_free;
    }
  }

 close_and_free:
  fclose(out);

 free:
  for (int i = 0; i < blocks; i++) {
    err = cudaFree(allocated_blocks[i]);
    cudaFatal(err);
  }

  free(allocated_blocks);

  free(host_buf);

  return exit_code;
}
