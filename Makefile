GPU_ARCH=sm_32

all: gdump gburn

gdump: gdump.cu
	nvcc -g -arch $(GPU_ARCH) gdump.cu -o gdump -lstdc++

gburn: gburn.cu
	nvcc -g -arch $(GPU_ARCH) gburn.cu -o gburn -lstdc++ -lcurses
