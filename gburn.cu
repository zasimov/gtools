/*
 * gburn writes the file to the GPU memory.
 */

#include <stdio.h>
#include <unistd.h>
#include <curses.h>

const size_t KB = 1024;
const size_t MB = 1024 * KB;

const size_t max_file_size = 512 * MB;


#define cudaFatal(expr) { __cudaFatal((expr), __FILE__, __LINE__); }
inline void __cudaFatal(cudaError_t err, const char *file, int lineno) {
  if (err != cudaSuccess) {
    fprintf(stderr, "CUDA: %s %s %d\n", cudaGetErrorString(err), file, lineno);
    exit(err);
  }
}


struct prefixed_buf {
  size_t len;
  void *data;
};


void free_prefixed_buf(struct prefixed_buf *p_buf) {
  free(p_buf->data);
}

struct prefixed_buf read_file(char *in_file) {
  FILE *f = NULL;
  size_t file_size = 0;
  int read = -1;
  void *buf = NULL;
  struct prefixed_buf p_buf;

  f = fopen(in_file, "rb");
  if (!f) {
    perror("fopen");
    exit(1);
  }

  fseek(f, 0, SEEK_END);
  file_size = ftell(f);
  fseek(f, 0, SEEK_SET);

  if (file_size > max_file_size) {
    fprintf(stderr, "ERROR: input file is too big. Max allowed size is %d.\n", max_file_size);
    exit(1);
  }

  buf = (void *)malloc(file_size);
  if (!buf) {
    free(buf);
    exit(1);
  }

  read = fread(buf, file_size, 1, f);
  if (read != 1) {
    perror("fread");
    free(buf);
    exit(1);
  }

  fclose(f);

  p_buf.len = file_size;
  p_buf.data = buf;

  return p_buf;
}

void fill_host_buf(char *host_buf, int host_buf_len, struct prefixed_buf *pattern) {
  size_t to_copy = 0;
  int remaining = host_buf_len;

  if (pattern->len >= host_buf_len) {
    memcpy(host_buf, pattern->data, host_buf_len);
  } else {
    while (remaining > 0) {
      to_copy = remaining < pattern->len ? remaining : pattern->len;
      memcpy(host_buf, pattern->data, to_copy);
      host_buf += to_copy;
      remaining -= to_copy;
    }
  }
}

int main(int argc, char *argv[]) {
  int c = '\0';
  char *in_file = NULL;
  int block_size = -1;
  void *host_buf = NULL;
  struct prefixed_buf pattern;
  size_t gpu_free = 0, gpu_total = 0;
  int blocks = 0;
  void **allocated_blocks = NULL;
  int allocated = 0;
  int exit_code = 0;
  cudaError_t err;
  char stop = 0;
  char ignore_err_memalloc = 0;

  while ((c = getopt(argc, argv, "smb:i:")) != -1) {
    switch (c) {
    case 'i':
      in_file = optarg;
      break;
    case 'b':
      block_size = atoi(optarg);
      break;
    case 's':
      stop = 1;
      break;
    case 'm':
      ignore_err_memalloc = 1;
      break;
    default:
      abort();
    }
  }

  if (!in_file) {
    fprintf(stderr, "ERROR: input file (-i) is required.\n");
    return 1;
  }

  if (block_size <= 0) {
    fprintf(stderr, "ERROR: block size must be a positive number.\n");
    return 1;
  }

  err = cudaMemGetInfo(&gpu_free, &gpu_total);
  cudaFatal(err);

  host_buf = (void *)malloc(block_size);
  if (!host_buf) {
    perror("malloc");
    return 1;
  }

  pattern = read_file(in_file);

  fill_host_buf((char *)host_buf, block_size, &pattern);

  blocks = gpu_free / block_size;
  allocated_blocks = (void **)calloc(blocks, sizeof(void *));
  if (!allocated_blocks) {
    perror("calloc");
    exit_code = 1;
    goto free;
  }

  for (int i = 0; i < blocks; i++) {
    err = cudaMalloc(&allocated_blocks[i], block_size);
    if (err == cudaErrorMemoryAllocation && ignore_err_memalloc) {
      fprintf(stderr, "cudaErrorMemoryAllocation ignored.\n");
      break;
    }
    cudaFatal(err);

    printf("%d/%d\n", i, blocks);

    err = cudaMemcpy(allocated_blocks[i], host_buf, block_size, cudaMemcpyHostToDevice);
    allocated++;
    cudaFatal(err);
  }

  printf("Allocated %d block(s).\n", allocated);

  for (int i = 0; i < allocated; i++) {
    cudaFree(allocated_blocks[i]);
  }

  free(allocated_blocks);

 free:
  free_prefixed_buf(&pattern);
  free(host_buf);


  if (stop) {
    printf("Waiting...\n");
    getchar();
  }

  return exit_code;
}